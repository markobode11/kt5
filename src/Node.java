import java.util.*;

public class Node {

    private String name;
    private Node firstChild;
    private Node nextSibling;

    Node(String n, Node d, Node r) {
        this.name = n;
        this.firstChild = d;
        this.nextSibling = r;
    }

    public void setFirstChild(Node c) {
        firstChild = c;
    }

    public void setNextSibling(Node s) {
        nextSibling = s;
    }

    public void setName(String n) {
        name = n;
    }

    public static Node parsePostfix(String s) {
        StringBuilder name = new StringBuilder();
        boolean partEnded = false;
        boolean skipNext = false;
        boolean isfirstLevel = true;
        int tier = 0;
        List<Node> currentNodes = new ArrayList<>();

        if (s.charAt(0) == '!') {
            isfirstLevel = false;
            s = s.substring(1);
        }

        if (s.contains("))")) {
            throw new RuntimeException("Postfix cannot contain double ending brackets '))'. Faulty postfix:" + s);
        }
        if (s.contains(",") && (!s.contains("(") || !s.contains(")"))) {
            throw new RuntimeException("If postfix contains commas it also has to contain brackets '()'. Faulty postfix:" + s);
        }
        if (s.contains("(,")) {
            throw new RuntimeException("Node name cannot be empty. Empty node name found in postfix:" + s);
        }

        for (int i = 0; i < s.length(); i++) {
            char currentChar = s.charAt(i);

            if (i != 0) {
                if (currentChar == ',' && s.charAt(i - 1) == ',') {
                    throw new RuntimeException("Node name cannot contain commas. Faulty postfix:" + s);
                }
                if (currentChar == ')' && s.charAt(i - 1) == '(') {
                    throw new RuntimeException("Subtree cannot be empty. Faulty postfix:" + s);
                }
            }

            if (!skipNext) {
                if (partEnded && (i == s.length() - 1 || currentChar == ',' || currentChar == ')')) {
                    if (i == s.length() - 1) {
                        name.append(currentChar);
                    }
                    if (isfirstLevel && i != s.length() - 1) {
                        throw new RuntimeException("Parent node cannot be found. There are several nodes on the highest level. Faulty postfix:" + s);
                    }
                    if (name.length() == 0) {
                        throw new RuntimeException("No node names found! Faulty postfix:" + s);
                    }
                    return new Node(name.toString(), currentNodes.get(0), null);
                } else if (currentChar == ')') {
                    if (name.length() != 0) {               // if name == 0  then program returned from recursion
                        Node node = new Node(name.toString(), null, null);
                        if (currentNodes.size() != 0) {
                            currentNodes.get(currentNodes.size() - 1).setNextSibling(node);
                        }
                        currentNodes.add(node);
                        name = new StringBuilder();
                    }
                    partEnded = true;
                } else if (currentChar == '(') {          // do not call new recursion if first char is (
                    if (i != 0) {
                        tier++;
                        Node node = parsePostfix("!" + s.substring(i));
                        if (currentNodes.size() != 0) {
                            currentNodes.get(currentNodes.size() - 1).setNextSibling(node);
                        }
                        currentNodes.add(node);
                        skipNext = true;
                    }
                } else if (currentChar == ',') {
                    if (name.length() != 0) {                       // if name == 0 then program returned from recursion
                        Node node = new Node(name.toString(), null, null);
                        if (currentNodes.size() != 0) {
                            currentNodes.get(currentNodes.size() - 1).setNextSibling(node);
                        }
                        currentNodes.add(node);
                        name = new StringBuilder();
                    }
                } else {
                    if (name.length() != 0 && currentChar == '\t') {
                        throw new RuntimeException("Node name cannot contain tabs. Faulty postfix:" + s);
                    }
                    if (name.length() != 0 && currentChar == ' ') {
                        throw new RuntimeException("Node name cannot contain spaces. Faulty postfix:" + s);
                    }
                    if (currentChar != ' ' && currentChar != '\t') {
                        name.append(currentChar);
                    }
                }
            } else {
                if (currentChar == '(') {
                    tier++;
                } else if (s.charAt(i - 1) == ')') {
                    tier--;
                }
                if (tier == 0) {
                    skipNext = false;
                }
            }
        }
        if (name.length() == 0) {
            throw new RuntimeException("No node names found! Faulty postfix:" + s);
        }
        if (currentNodes.size() == 0) {
            return new Node(name.toString(), null, null);
        }
        return new Node(name.toString(), currentNodes.get(0), null);
    }

    public String leftParentheticRepresentation() {

        StringBuilder builder = new StringBuilder();

        if (firstChild == null && nextSibling != null) {
            builder.append(name)
                    .append(",")
                    .append(nextSibling.leftParentheticRepresentation());
        } else if (firstChild == null) {
            builder.append(name);
        } else if (nextSibling == null) {
            builder.append(name)
                    .append("(")
                    .append(firstChild.leftParentheticRepresentation())
                    .append(")");
        } else {
            builder.append(name)
                    .append("(")
                    .append(firstChild.leftParentheticRepresentation())
                    .append("),")
                    .append(nextSibling.leftParentheticRepresentation());
        }
        return builder.toString();
    }

    public String treeToPseudoXML(int... tier) {
        StringBuilder builder = new StringBuilder();
        int currentTier = tier.length > 0 ? tier[0] : 1;

        StringBuilder tabs = new StringBuilder();
        tabs.append("\t".repeat(Math.max(0, currentTier - 1)));

        String start = tabs.toString() + "<L" + currentTier + "> ";
        String end1 = "</L" + currentTier + ">";
        String end2 = " </L" + currentTier + ">";

        builder.append(start).append(name);

        if (firstChild == null && nextSibling != null) {
            builder.append(end2)
                    .append("\n")
                    .append(nextSibling.treeToPseudoXML(currentTier));
        } else if (firstChild == null) {
            builder.append(end2);
        } else if (nextSibling == null) {
            builder.append("\n")
                    .append(firstChild.treeToPseudoXML(currentTier + 1))
                    .append("\n")
                    .append(tabs.toString())
                    .append(end1);
        } else {
            builder.append("\n")
                    .append(firstChild.treeToPseudoXML(currentTier + 1))
                    .append("\n")
                    .append(tabs.toString())
                    .append(end1)
                    .append("\n")
                    .append(nextSibling.treeToPseudoXML(currentTier));
        }
        return builder.toString();
    }

    public static void main(String[] param) {
        String s = "((((E)D)C)B)A";
        Node t = Node.parsePostfix(s);
        String v = t.treeToPseudoXML();
        System.out.println(v); // (B1,C)A ==> A(B1,C)
    }
}

